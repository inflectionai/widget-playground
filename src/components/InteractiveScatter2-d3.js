import * as d3 from 'd3';
import * as math from 'mathjs';

export default function(svg, width, height) {


function randomData(samples) {
  var data = [];

  for (var i = 0; i < samples; i++) {
    data.push({
      x: i,
      y: i%12 === 0 && i !== 0 ? 50 : i+10*Math.random()
    });
  }

  data.sort(function(a, b) {
    return a.x - b.x;
  })
  return data;
}

var data = randomData(10);
var lineData = [];

// Set Color Scale
// let color = d3.scaleOrdinal(d3.schemeCategory20);

/*********************** Plot Below *********************/
var margin = {
  top: 20,
  right: 20,
  bottom: 30,
  left: 50
};

// var width = 800 - margin.left - margin.right,
//   height = 400 - margin.top - margin.bottom;

var xScale = d3.scaleLinear()
  .range([0, width])
  .domain(d3.extent(data, function(d) {
    return d.x;
  })).nice();

var yScale = d3.scaleLinear()
  .range([height, 0])
  .domain(d3.extent(data, function(d) {
    return d.y;
  })).nice();

var xAxis = d3.axisBottom(xScale).ticks(12),
  yAxis = d3.axisLeft(yScale).ticks(12 * height / width);

var plotLine = d3.line()
  .x(function(d) {
    return xScale(d.x);
  })
  .y(function(d) {
    return yScale(d.y);
  });

d3.select("#plot").append("svg")
  .attr("width", width + margin.left + margin.right)
  .attr("height", height + margin.top + margin.bottom);

svg.append("g")
  .attr("class", "x axis ")
  .attr('id', "axis--x")
  .attr("transform", "translate(" + margin.left + "," + (height + margin.top) + ")")
  .call(xAxis);

svg.append("g")
  .attr("class", "y axis")
  .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
  .attr('id', "axis--y")
  .call(yAxis);

var clickZone = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")")
	.append("g").append("rect")
    .attr("class", "click-zone")
    .attr("width", width)
    .attr("height", height)
    .style("opacity", 0)
    .on("click", function() {
    	let clickArea = d3.select(this).node();
      
      addPoint(clickArea);			
      update();
    });
  
var line = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

var dot = svg.append("g")
  .attr("id", "scatter")
  .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

// Add line plot
line.append("g")
    .attr("id", "line-1")
    .attr("clip-path", "url(#clip)")
      .append("path")
      .data([lineData])
      .attr("class", "pointlines")
      .attr("d", plotLine)
      .style("fill", "none")
      .style("stroke", "brown");
    
dot.append("g")
  .attr("id", "scatter-1")
  .attr("clip-path", "url(#clip)")
  .selectAll(".dot")
  .data(data)
    .enter().append("circle")
    .attr("class", "dot")
    .attr("r", 5)
    .attr("cx", function(d) {
      return xScale(d.x);
    })
    .attr("cy", function(d) {
      return yScale(d.y);
    })
    .attr("stroke", "white")
    .attr("stroke-width", "2px")
    .style("fill", function(d) {
    	return d.y > 45 ? "gold" : "brown";
    })
    .on("click", function(d,i) {
    	let s = d3.select(this);
      remove(s,i);
    });

dot.append("g")
  .attr("id", "scatter-1")
  .attr("clip-path", "url(#clip)")
  .selectAll(".dot")
  .data([lineData])
    .enter().append("circle")
    .attr("class", "dot")
    .attr("r", 5)
    .attr("cx", function(d) {
      return xScale(d.x);
    })
    .attr("cy", function(d) {
      return yScale(d.y);
    })
    .attr("stroke", "white")
    .attr("stroke-width", "2px")
    .style("fill", function(d) {
      return d.y > 45 ? "gold" : "brown";
    })
    .on("click", function(d,i) {
      let s = d3.select(this);
      remove(s,i);
    });

/************ Add/Remove Point Below **************************/
function addPoint(clickArea) {
			var pos = d3.mouse(clickArea);
      var xPos = xScale.invert(pos[0]);
      var yPos = yScale.invert(pos[1]);
      
      lineData.push({x: xPos, y: yPos});
      lineData.sort(function(a,b) { return a.x - b.x});
}

function remove(sel,index) {	 
  lineData.splice(index,1);
  update();
}
  
/************ Update Below **************************/
function update() {  
  // update line
  console.log('line data:', lineData);
  
  // Update line

  d3.select("#line-1").select("path")
    .data([lineData])
    .transition().duration(1000)
    .attr("d", plotLine);
    
   // Remove old line data
   d3.select("#line-1").select("path").data([lineData]).exit().remove();
   	
  //Update all circles
  // d3.select("#scatter-1").selectAll("circle")
  //   .data(lineData)
  //   .transition()
  //   .duration(1000)
  //   .attr("cx", function(d) {
  //     return xScale(d.x);
  //   })
  //   .attr("cy", function(d) {
  //     return yScale(d.y);
  //   })
  //   .style("fill", function(d) {
  //   	return d.y > 45 ? "gold" : "brown";
  //   });

  //Enter new circles
  d3.select("#scatter-1").selectAll(".dot")
    .data(lineData)
    .enter()
    .append("circle")
    .attr("class", "dot")
    .attr("cx", function(d) {
      return xScale(d.x);
    })
    .attr("cy", function(d) {
      return yScale(d.y);
    })
    .attr("r", 5)
    .attr("stroke", "white")
    .attr("stroke-width", "2px")
    .style("fill", function(d) {
    	return d.y > 45 ? "gold" : "brown";
    })
    .on("click", function(d,i) {
    	let s = d3.select(this);
      // remove(s,i);
    });

  // Remove old
  // d3.selectAll("circle")
  //   .data(data)
  //   .exit()
  //   .remove()
}
}