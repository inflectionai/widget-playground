import React, { useState } from 'react';
import { A } from 'hookrouter';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  NavItem
} from 'reactstrap';

function NavHeader () {
  const [isOpen, setIsOpen] = useState(false)

  function toggle() {
    setIsOpen(!isOpen)
  }

  return (
    <div>
      <Navbar color="light" light expand="md">
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="ml-auto" navbar>
            <NavItem>
              <A href="/bar-scatter" className='nav-link'>Bar/Scatter</A>
            </NavItem>
            <NavItem>
              <A href="/interactive-scatter-1" className='nav-link'>Interactive Scatter 1</A>
            </NavItem>
            <NavItem>
              <A href="/interactive-scatter-2" className='nav-link'>Interactive Scatter 2</A>
            </NavItem>
            <NavItem>
              <A href="/interactive-scatter-3" className='nav-link'>Interactive Scatter 3</A>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    </div>
  );
}

export default NavHeader;