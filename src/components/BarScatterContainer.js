import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import AllDataColumns from './AllDataColumns';
import Histogram from './Histogram';
import ScatterPlot from './ScatterPlot';


const dummyData = [
  {'descriptor': '# of dogs', 'data': [1, 3, 7, 4]},
  {'descriptor': '# of cats', 'data': [5, 2, 11, 0]},
  {'descriptor': '# of happiness', 'data': [15, 12, 0, 4]}
]

function BarScatterContainer() {
  const columnData = dummyData;
  const [activeColumns, setActiveColumns] = useState([]);

  const handleColumnClick = function(column, activate) {
    activate
    ? setActiveColumns(activeColumns => [...activeColumns, column])
    : setActiveColumns(activeColumns => activeColumns.filter(col => col !== column))
  }

  return (
    <div className="App">
      <h1>Bar + Scatter Widget</h1>
      <div>Description: Click 1 column shows histogram, Click 2 columns shows scatter.  Click column again to deselect</div>
      <AllDataColumns handleColumnClick={handleColumnClick} columnData={columnData}/>
      <header>Bar/Scatter Display:</header>
      {activeColumns.length === 1 && <Histogram column={activeColumns[0]}/>}
      {activeColumns.length === 2 && <ScatterPlot columns={activeColumns}/>}
    </div>
  );
}

export default BarScatterContainer;
