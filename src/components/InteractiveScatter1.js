import React, { useEffect, useRef } from 'react';
import * as d3 from 'd3';
import InteractiveScatter from './InteractiveScatter1-d3';

const D3blackbox = ({ x, y, render }) => {
  const refAnchor = useRef(null);

  useEffect(() => {
    render(d3.select(refAnchor.current))
  });

  return <g ref={refAnchor} transform={`translate(${x}, ${y})`} />;
}

function InteractiveScatter1() {
  return (
    <div className="App">
      <h1>Reactive best fit line</h1>
      <div>Description: Click anywhere to add new points.  Line of best fit will re-render.  Click points to remove them.</div>
      <svg width='900' height='900'>
        <D3blackbox x={0} y={0} render={svg => InteractiveScatter(svg, 600, 600)} />
      </svg>
    </div>
  );
}

export default InteractiveScatter1;
