import React, { useEffect, useRef } from 'react';
import * as d3 from 'd3';
import InteractiveScatter from './InteractiveScatter2-d3';

const D3blackbox = ({ x, y, render }) => {
  const refAnchor = useRef(null);

  useEffect(() => {
    render(d3.select(refAnchor.current))
  });

  return <g ref={refAnchor} transform={`translate(${x}, ${y})`} />;
}

function InteractiveScatter2() {
  return (
    <div className="App">
      <h1>Draw best fit line</h1>
      <div>Description: Click multiple points to draw a best fit line.  Line will draw after atleast 2 points are clicked.</div>
      <svg width='900' height='900'>
        <D3blackbox x={0} y={0} render={svg => InteractiveScatter(svg, 600, 600)} />
      </svg>
    </div>
  );
}

export default InteractiveScatter2;
