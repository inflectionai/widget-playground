import React from 'react';
import './App.css';
import NavHeader from './components/NavHeader'
import BarScatterContainer from './components/BarScatterContainer'
import InteractiveScatter1 from './components/InteractiveScatter1'
import InteractiveScatter2 from './components/InteractiveScatter2'
import InteractiveScatter3 from './components/InteractiveScatter3'
import { useRoutes } from 'hookrouter'
 
const routes = {
    '/bar-scatter': () => <BarScatterContainer />,
    '/interactive-scatter-1': () => <InteractiveScatter1 />,
    '/interactive-scatter-2': () => <InteractiveScatter2 />,
    '/interactive-scatter-3': () => <InteractiveScatter3 />
};

function App() {
  const routeResult = useRoutes(routes);

  return (
    <div className="App">
      <NavHeader />
      { routeResult }
    </div>
  );
}

export default App;
